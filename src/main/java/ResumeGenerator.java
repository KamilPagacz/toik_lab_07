import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;


public class ResumeGenerator {
    private static String FILE = "./Resume.pdf";
    private static Font headerFont = new Font(Font.FontFamily.COURIER, 24, Font.BOLD);
    private static Font tableFont = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL);

    protected static String getFilePath(){
        return FILE;
    }

    protected static void addMetaData(Document document) {
        document.addTitle("My first PDF");
        document.addSubject("Using iText");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("Kamil Pagacz");
        document.addCreator("Kamil Pagacz");
    }

    protected static void addHeader(Document document) throws DocumentException {
        Paragraph preface = new Paragraph();

        // Resume header
        Paragraph resume = new Paragraph("Resume", headerFont);
        resume.setAlignment(Element.ALIGN_CENTER);
        preface.add(resume);

        addEmptyLine(preface, 5);

        document.add(preface);
    }

    protected static void addContent(Document document) throws DocumentException {
        Paragraph tableParagraph = new Paragraph();
        createResumeTable(tableParagraph);
        document.add(tableParagraph);

    }

    protected static void createResumeTable(Paragraph para) {
        PdfPTable table = new PdfPTable(2);

        PdfPCell cell = new PdfPCell();
        cell.setPadding(5);
        cell.setPaddingBottom(10);

        cell.setPhrase(new Phrase("First Name", tableFont));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Kamil", tableFont));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Last Name", tableFont));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Pagacz", tableFont));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Education", tableFont));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Bla\nbla\nbla", tableFont));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Summary", tableFont));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Bla\nbla\nbla\nbla", tableFont));
        table.addCell(cell);

        para.add(table);
    }

    protected static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}