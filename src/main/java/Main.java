import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;

public class Main {

    public static void main (String[] args){
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(ResumeGenerator.getFilePath()));
            document.open();
            ResumeGenerator.addMetaData(document);
            ResumeGenerator.addHeader(document);
            ResumeGenerator.addContent(document);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
